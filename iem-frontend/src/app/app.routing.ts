import { Routes } from '@angular/router';
import { CalendarComponent } from './calendar/calendar.component';
import { LoginComponent } from './login/login.component';

export const appRoutes: Routes = [
    {
        path: '',
        component: CalendarComponent
    },
    {
        path: 'events',
        loadChildren: './events/events.module#EventsModule'
    },
    {
        path: 'login',
        component: LoginComponent
    }
];