import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { LoginService } from './login.service';
import { NavService } from '../core/nav.service';

@Component({
  selector: 'iem-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  signinForm: FormGroup;

  constructor(private fb: FormBuilder, private loginService: LoginService, private navService: NavService) {
    this.signinForm = this.fb.group({
      id: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      remember: new FormControl(false)
    })
  }

  ngOnInit() {
  }

  signin() {
    let form = this.signinForm.value;
    if(this.loginService.login(form.id, form.password, form.remember)){
      this.navService.goToHomePage();
    }
  }
}
