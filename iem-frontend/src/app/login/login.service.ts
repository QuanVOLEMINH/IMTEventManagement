import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { NavService } from '../core/nav.service';
import { TokenService } from '../core/token.service';

@Injectable({
    providedIn: 'root',
})
export class LoginService {

    constructor(private navService: NavService, private tokenService: TokenService) {
    }

    login(id: string, password: string, remember: boolean) {
        if (id == "quan" && password == "quan") {
            this.navService.goToHomePage();
            this.tokenService.addLoginSession(id, "VO LE MINH Quan", remember);
            return true;
        } else if (id == "quynh" && password == "quynh") {
            this.navService.goToHomePage();
            this.tokenService.addLoginSession(id, "Quynh", remember);
            return true;
        } else {
            return false;
        }

    }
}