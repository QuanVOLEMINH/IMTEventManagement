import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { IMTEvent } from '../models/event';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class EventService {
    baseUri: string;

    constructor(private http: HttpClient) {
        this.baseUri = environment.baseUri;
    }

    addEvent(event: IMTEvent, author: string) {
        event.author = author;
        let headers = new HttpHeaders();
        headers.append("Content-Type", "application/json");
        return this.http.post(this.baseUri + "/events/create", event, { headers: headers });
    }

    getEvents() {
        return this.http.get<any>(this.baseUri + "/events").pipe(
            map((events) => {
                return events.map(event => {
                    return {
                        id: event._id,
                        title: event.title,
                        start: new Date(event.start),
                        end: new Date(event.end),
                        organizer: event.organizer,
                        description: event.description,
                        author: event.author,
                        color: {
                            primary: this.getRandomColor(),
                            secondary: this.getRandomColor()
                        },
                    }
                })
            })
        );
    }

    deleteEvent(eventId) {
        return this.http.post(this.baseUri + "/events/delete", { id: eventId });
    }

    updateEvent(event) {
        let headers = new HttpHeaders();
        headers.append("Content-Type", "application/json");
        return this.http.post(this.baseUri + "/events/update", event, { headers: headers });
    }

    private getRandomColor() {
        const letters = '0123456789ABCDEF';
        let color = '#';
        for (let i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
}