import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { EventService } from './events.service';
import { IMTEvent } from '../models/event';
import { TokenService } from '../core/token.service';

@Component({
    selector: 'iem-create-event',
    templateUrl: './list-event.component.html',
    styleUrls: ['./list-event.component.scss']
})
export class ListEventComponent implements OnInit {
    events$: Observable<IMTEvent[]>;
    editState: boolean;
    eventToEdit;
    refresh: Subject<any> = new Subject();
    today: Date = new Date();

    constructor(private eventService: EventService, public tokenService: TokenService) {
    }

    ngOnInit() {
        this.refreshData();
        this.editState = false;
    }

    deleteEvent(eventId) {
        if (confirm("Do you want to delete this event?")) {
            this.eventService.deleteEvent(eventId).subscribe(
                (message: any) => {
                    if (message.success) {
                        this.refreshData();
                    }
                },
                error => console.log(error)
            );
        }
    }

    editEvent(event) {
        if (this.editState) {
            this.editState = false;
            this.eventToEdit = null;
        }
        else {
            this.editState = true;
            this.eventToEdit = event;
        }
    }

    updateEvent() {
        console.log(this.eventToEdit);
        if (this.eventToEdit.start > this.eventToEdit.end || !this.eventToEdit.title || !this.eventToEdit.organizer || !this.eventToEdit.description) {
            console.log("not allow");
        } else {
            if (confirm("Do you want to update this event")) {
                this.eventService.updateEvent(this.eventToEdit).subscribe(
                    (message: any) => {
                        if (message.success) {
                            console.log(message);
                            this.editState = false;
                            this.eventToEdit = null;
                        }
                    },
                    (error) => console.log(error)
                );
            }
        }
    }
    refreshData() {
        this.events$ = this.eventService.getEvents();
    }

}
