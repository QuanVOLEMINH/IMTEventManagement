import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { startOfDay, endOfDay } from 'date-fns';
import { EventService } from './events.service';
import { Router } from '@angular/router';
import { TokenService } from '../core/token.service';

@Component({
  selector: 'iem-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.scss']
})
export class CreateEventComponent implements OnInit {

  refresh: Subject<any> = new Subject();
  eventForm: FormGroup;
  today: Date;

  constructor(private fb: FormBuilder, private eventService: EventService, private router: Router, public tokenService: TokenService) {
    this.eventForm = this.fb.group({
      title: new FormControl('', Validators.required),
      start: new FormControl(new Date(), Validators.required),
      end: new FormControl(endOfDay(new Date()), Validators.required),
      organizer: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
    });
    this.today = startOfDay(new Date());
  }

  ngOnInit() {
  }

  create() {
    if (this.eventForm.value.start >= this.eventForm.value.end) {
      console.log("not valid");
    } else {
      if (window.confirm("Do you want to create this event")) {
        this.eventService.addEvent(this.eventForm.value, this.tokenService.getCurrentUser().name).subscribe(
          res => {
            console.log(res);
          this.goToHomePage();
          },
          err => console.log(err)
        );
      } else {
        console.log("not ok");
      }
    }
  }

  goToHomePage() {
    this.router.navigate(['']);
  }
}
