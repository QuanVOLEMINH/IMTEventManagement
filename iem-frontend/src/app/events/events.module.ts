import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { CreateEventComponent } from './create-event.component';
import { EventService } from './events.service';
import { FlatpickrModule } from 'angularx-flatpickr';
import { ListEventComponent } from './list-event.component';

const routes: Routes = [
    {
        path: 'create',
        component: CreateEventComponent
    },
    {
        path: 'list',
        component: ListEventComponent
    }
];

@NgModule({
    declarations: [
        CreateEventComponent,
        ListEventComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(routes),
        FlatpickrModule.forRoot(),
        ReactiveFormsModule
    ],
    exports: [],
    providers: [EventService]
})
export class EventsModule { }
