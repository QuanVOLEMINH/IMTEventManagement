import { Component } from '@angular/core';

@Component({
  selector: 'iem-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'IMT Events';
  year = new Date().getFullYear();
}
