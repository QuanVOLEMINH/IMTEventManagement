export class IMTEvent{
    title: string;
    start: Date;
    end: Date;
    organizer: string;
    description: string;
    author: string;
}