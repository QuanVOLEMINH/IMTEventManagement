import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CalendarComponent } from './calendar.component';
import { NgbModalModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlatpickrModule } from 'angularx-flatpickr';
import { CalendarService } from './calendar.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    declarations: [
        CalendarComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        FlatpickrModule.forRoot(),
        CalendarModule.forRoot({
            provide: DateAdapter,
            useFactory: adapterFactory
        }),
        BrowserAnimationsModule,
        NgbModalModule,
        HttpClientModule 
    ],
    exports: [],
    providers: [CalendarService]
})
export class MyCalendarModule { }
