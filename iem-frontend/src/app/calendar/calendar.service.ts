import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { CalendarEventAction, CalendarEvent } from 'angular-calendar';

@Injectable({
    providedIn: 'root',
})
export class CalendarService {
    baseUri: string;

    colors: any = {
        red: {
            primary: '#ad2121',
            secondary: '#FAE3E3'
        },
        blue: {
            primary: '#1e90ff',
            secondary: '#D1E8FF' // default hover color
        },
        yellow: {
            primary: '#e3bc08',
            secondary: '#FDF1BA'
        }
    };

    constructor(private http: HttpClient) {
        this.baseUri = environment.baseUri;
    }

    getEvents() {
        return this.http.get<any>(this.baseUri + "/events").pipe(
            map((events) => {
                return events.map(event => {
                    return {
                        title: event.title,
                        start: new Date(event.start),
                        end: new Date(event.end),
                        organizer: event.organizer,
                        description: event.description,
                        author: event.author,
                        color: {
                            primary: this.getRandomColor(),
                            secondary: this.getRandomColor()
                        },
                    }
                })
            })
        );
    }

    private getRandomColor() {
        const letters = '0123456789ABCDEF';
        let color = '#';
        for (let i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }


}