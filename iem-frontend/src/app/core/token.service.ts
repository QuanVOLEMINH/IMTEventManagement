import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
    providedIn: 'root',
})
export class TokenService {

    constructor(private router: Router, private cookieService: CookieService) {

    }

    addLoginSession(id: string, name: string, remember: boolean) {
        if (this.cookieService.check('id')) {
            this.clearSession();
        }

        let expires = new Date();
        expires.setHours(expires.getHours() + 1);

        this.cookieService.set('id', id);
        this.cookieService.set('name', name);
        this.cookieService.set('remember', remember + '');
        this.cookieService.set('expires', expires + '');

    }

    isValid() {
        if (this.cookieService.check("expires")) {
            return new Date() < new Date(this.cookieService.get("expires"));
        }
        return false;
    }

    getCurrentUser() {
        if (this.cookieService.check("id")) {
            return {
                id: this.cookieService.get('id'),
                name: this.cookieService.get('name'),
                remember: this.cookieService.get('remember'),
                expires: this.cookieService.get("expires")
            };
        }
        return null;
    }

    clearSession() {
        this.cookieService.deleteAll('/');
    }
}