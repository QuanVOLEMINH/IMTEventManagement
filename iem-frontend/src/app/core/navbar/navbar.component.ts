import { Component, OnInit } from '@angular/core';
import { TokenService } from '../token.service';
import { LogOutService } from '../logout.service';

@Component({
  selector: 'iem-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(public tokenService: TokenService, private logOutService: LogOutService) { }

  ngOnInit() {
  }

  logOut() {
    this.logOutService.logout();
  }

}
