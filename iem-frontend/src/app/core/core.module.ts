import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NavService } from './nav.service';
import { CookieService } from 'ngx-cookie-service';
import { LogOutService } from './logout.service';

@NgModule({
    declarations: [
        NavbarComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        RouterModule
    ],
    exports: [NavbarComponent],
    providers: [NavService, CookieService, LogOutService]
})
export class CoreModule { }
