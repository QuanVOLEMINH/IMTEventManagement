import { Injectable } from '@angular/core';
import { NavService } from './nav.service';
import { TokenService } from './token.service';

@Injectable({
    providedIn: 'root',
})
export class LogOutService {

    constructor(private navService: NavService, private tokenService: TokenService) {

    }

    logout(){
        this.tokenService.clearSession();
        this.navService.goToLoginPage();
    }
}