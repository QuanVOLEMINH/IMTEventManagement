import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root',
})
export class NavService {

    constructor(private router: Router) {

    }

    goToHomePage() {
        this.router.navigate(['']);
    }

    goToLoginPage() {
        this.router.navigateByUrl('/', { skipLocationChange: true })
            .then(() =>
                this.router.navigate(['/login'])
            );
    }

    goToCreatePage() {
        this.router.navigate(['/events/create']);
    }
}