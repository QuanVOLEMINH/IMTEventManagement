const express = require('express');
const app = express();
const port = process.env.PORT || 8080;

// import middleware
const bodyParser = require('body-parser');
const cors = require('cors');

// routes
const events = require("./routes/events");

// database
const mongoose = require('mongoose');
//  database config
const env = process.env.NODE_ENV || 'development';
let connectionUrl;
if (env === 'development') {
    const databaseConfig = require("./config/database");
    connectionUrl = databaseConfig.url;
} else {
    connectionUrl = process.env.DATABASE_URL;
}
mongoose.connect(connectionUrl, { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.connection.on('connected', () => {
    console.log("Connected to database: " + mongoose.connection.name);
});
mongoose.connection.on('error', () => {
    console.log("Database error: " + mongoose.connection.name);
});

// use middleware
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(cors());

// use routes
app.use('/events', events);

// default path
app.get('*', function (request, response) {
    response.sendFile(__dirname + '/public/index.html');
});

const listener = app.listen(port, function () {
    console.log('Service is running on port: ' + listener.address().port);
});