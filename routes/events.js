const express = require('express');
const router = express.Router();
const IMTEvent = require('../models/event');

router.get('/', (req, res, next) => {
    console.log("Get all events called");
    IMTEvent.getEvents((error, events) => {
        error ? res.status(404).send("not found") : res.status(200).send(events);
    });
});

router.post('/create', (req, res, next) => {
    let newEvent = new IMTEvent({
        title: req.body.title,
        start: req.body.start,
        end: req.body.end,
        organizer: req.body.organizer,
        description: req.body.description,
        author: req.body.author,
    });

    IMTEvent.addEvent(newEvent, (error, event) => {
        error ? res.status(500).json({ success: false, msg: "Failed to add event" }) : res.status(200).json({ success: true, msg: "Event added successfully" });
    })
});

router.post('/delete', (req, res, next) => {
    let dEvent = IMTEvent.getEventById(req.body.id);
    dEvent.deleteOne((error) => {
        error ? res.status(500).json({ success: false, msg: "Failed to delete event" }) : res.status(200).json({ success: true, msg: "Event deleted successfully" });
    })
});

router.post('/update', (req, res, next) => {
    IMTEvent.findById(req.body.id, (err, event) => {
        if (err) next();
        event.title = req.body.title;
        event.start = req.body.start;
        event.end = req.body.end;
        event.organizer = req.body.organizer;
        event.description = req.body.description;

        event.save(
            (error, updatedEvent) => {
                error ? res.status(500).json({ success: false, msg: "Failed to update event" }) : res.status(200).json({ success: true, msg: "Event updated successfully" });
            }
        )
    });
});

router.get('/:id', (req, res, next) => {
    console.log(`Get event with id=${id}.`);
    IMTEvent.getEventById(req.params.id, (error, event) => {
        error ? res.status(404).send("not found") : res.status(200).send(event);
    });
});

module.exports = router;