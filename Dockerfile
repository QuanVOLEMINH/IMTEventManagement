FROM node:12-alpine as Build_Frontend
WORKDIR /iem-frontend
ADD /iem-frontend ./
RUN npm install && npm run-script build-prod

FROM node:12-alpine
WORKDIR /
COPY . .
RUN npm install
COPY --from=Build_Frontend /public ./public/
EXPOSE 8080
CMD [ "node", "server.js" ]