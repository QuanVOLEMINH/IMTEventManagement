const mongoose = require('mongoose');
// const databaseConfig = require('../config/database');

const eventSchema = mongoose.Schema({
    title: {
        type: String,
        require: true
    },
    start: {
        type: Date,
        require: true
    },
    end: {
        type: Date,
        require: true
    },
    organizer: {
        type: String,
        require: true
    },
    description: {
        type: String,
        require: true
    },
    author: {
        type: String,
        require: true
    }
});

const IMTEvent = module.exports = mongoose.model('IMTEvent', eventSchema);

module.exports.getEvents = (callback) => {
    return IMTEvent.find(callback);
}

module.exports.getEventById = (id, callback) => {
    return IMTEvent.findById(id, callback);
}

module.exports.addEvent = (newEvent, callback) => {
    return newEvent.save(newEvent, callback);
}